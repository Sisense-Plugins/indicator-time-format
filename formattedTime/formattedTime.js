
//	Initialize the copyWidget object
//prism.run(["$filter", function($filter) {
	
	function formatAMPM(date) {
		var hours = date.getHours();
		var minutes = date.getMinutes();
		var seconds = date.getSeconds();
		var ampm = hours >= 12 ? 'PM' : 'AM';
		hours = hours % 12;
		hours = hours ? hours : 12; // the hour '0' should be '12'
		minutes = minutes < 10 ? '0'+minutes : minutes;
		seconds= seconds< 10 ? '0'+seconds : seconds;
		var strTime = hours + ':' + minutes + ':' + seconds + ' '+ ampm;
		return strTime;
	};
	
	
	function createDate(asStr,type){
		
		//	Pick apart the date pieces
		var y = asStr.slice(0,4),
			m = asStr.slice(4,6),
			d = asStr.slice(6,8),
			h = asStr.slice(8,10),
			min = asStr.slice(10,12),
			s = asStr.slice(12,14)
			
		//	Create a date object
		var newDate = new Date(y,m-1,d,h,min,s);
		
		if (type == "day"){
			var d = newDate.getDay();
			console.log('---'+ d);
			return d
		} else if (type == "time"){
			return formatAMPM(newDate);
		}
	};

	var toDoubleDigit = function (x) {
		if (x < 10) {
			return '0' + x;
		}
		return x
	};

	var updateFormatFactory = function(elt, newString) {
		return function() {
			
			elt.innerHTML = newString;
		}
	};

	var formatDuration = function(num, format) {
		
		var sign = num < 0 ? "-" : "";
		num = Math.abs(num);

		var showHours = false;
		var showMinutes = false;
		var showSeconds = false;
		var hours = 0
		var seconds = 0;
		var minutes = 0;

		if (format.indexOf('H') > -1) {
			showHours = true;
			hours = (parseInt(num / 3600));
			num = num % 3600;
		}
		minutes = parseInt(num / 60);
		if (format.indexOf('S') != -1) {
			
			showSeconds = true
			seconds = num % 60;
			var decimal_at = format.indexOf('.');
			if (decimal_at == -1) {
				seconds = seconds.toFixed(0);
			} else {
				seconds = seconds.toFixed(format.length - decimal_at - 1)
			}
		}

		var newVal = '';
		if (showHours) {
			newVal += toDoubleDigit(hours) + defaultSettings.timeSeparator
		}
		newVal += toDoubleDigit(minutes)
		if (showSeconds) {
			newVal += defaultSettings.timeSeparator + toDoubleDigit(seconds);
		}

		return newVal;
	};

	var MONTH_NAMES = ['INVALID', 'Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
	var MONTH_FULL_NAME = ['INVALID','January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];
	var DAY_NAMES = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'];
	
	// formatDate() will accept any format string composed of YYYY, YY, MMM, MM, DD values,
	// so it will work with '''YYYY-DD-MM''' as well as '''this DD date of MMMM in the year YYYY'''
	var formatDate = function(asInt, format) {
		var asStr = asInt.toString();
		var parts = {
			'YY': asStr.slice(0,4),
			'Y': asStr.slice(2,4),
			'MM': asStr.slice(4,6),
			'MMM': MONTH_NAMES[Number(asStr.slice(4,6))],
			'MName' : MONTH_FULL_NAME[Number(asStr.slice(4,6))],
			'Day' : DAY_NAMES[createDate(asStr,'day')],
			'DD': asStr.slice(6,8),
			'year' : asStr.slice(0,4),
			'Time' : createDate(asStr,'time'),
			'T': createDate(asStr,'time').slice(0,5) + ' ' +createDate(asStr,'time').slice(9,11),
			'ampm' : createDate(asStr,'time').slice(9,11)
		};

		var pieces = ["YY", "MMM", "Y", "MM", "DD", "Day", "MName", "year", "Time", "T", "ampm"];
		// Build out the new value.
		pieces.forEach(function (s) {
			format = format.replace(s,parts[s]);
		});

		return format;
	};

	//////////////////////////
	//	Plugin properties	//
	//////////////////////////

	//	Define allowed chart types
	var supportedChartTypes = ["indicator"];
	var FORMAT_SPEC = 'alternateFormatType';
    var none = 'none';


	var MENU_ITEMS = {
		'Seconds to Duration': [none, 'HH:MM', 'HH:MM:SS', 'HH:MM:SS.SS', 'MM:SS', 'MM:SS.SS'],
		'Integer to Date': [none, 'Day MName DD YY','Day MName DD YY Time','Day MName DD YY T','MMM DD YY','MM-DD-Y','DD/MM/YY', 'YY-MMM-DD','MMM-DD-Y','Day', 'MMM', "MName", "year", "Time",  'T', 'ampm']
	};


	//	Default settings
	var defaultSettings = {
		enabled : false,
		thousandsSeparator: ',',
        timeSeparator: ':'
	};

	//////////////////////////
	//	Utility Functions	//
	//////////////////////////

	//	Function to determine if this chart type is supported
	function widgetIsSupported(type){
		if (supportedChartTypes.indexOf(type) >= 0) {
			return true;
		} else {
			return false;
		}
	}


    var formatTime = function(se, ev) { // widget, args
        var widgetid = se.oid;
		//widget = $("widget[widgetid='" + widgetid + "']");

		var ps = [
			{
				panelNo: 0,
				itemNo: 0,
				dataId: 'value',
				eltSelector: ".number_span"
			},
			{
				panelNo: 1,
				itemNo: 0,
				dataId: 'secondary',
				eltSelector: ".secondary_span"
			}
		];

		ps.forEach(function (p) {
			var format = ev.widget.metadata.panels[p.panelNo].items.length>0 ?  ev.widget.metadata.panels[p.panelNo].items[p.itemNo][FORMAT_SPEC] : null;
			if ( format && format != 'none') {

				// widget will be [] if we are in widget edit mode.
				// in this case, we grab ''e'' as an unqualified jQuery selector
				/*
				var e = $("indicator.widget-body", widget);
				if (e.length == 0) {
					e = $("indicator.widget-body");
				}
				
				var elt = $(p.eltSelector, e)[0];
				*/
	
				var num = ev.widget.queryResult[p.dataId].data;

				var newVal = '';
				var newMin = '';
				var newMax = ''

				//	Are we displaying a guage?
				var isGauge = (ev.widget.queryResult.gauge) && (ev.widget.queryResult.min) && (ev.widget.queryResult.max);

				// Do we have a duration format
				if (MENU_ITEMS['Seconds to Duration'].indexOf(format) != -1) {
					newVal = formatDuration(num, format);
					
					if (isGauge) {
						newMin = formatDuration(ev.widget.queryResult.min.data,format);
						newMax = formatDuration(ev.widget.queryResult.max.data,format)
					}
					//setTimeout(updateFormatFactory(elt, formatDuration(num, format)))
				} else { // It's an integer to be formatted as a date
					newVal = formatDate(num, format);
					
					if (isGauge) {
						newMin = formatDate(ev.widget.queryResult.min.data,format);
						newMax = formatDate(ev.widget.queryResult.max.data,format)
					}
					//setTimeout(updateFormatFactory(elt, formatDate(num, format)))
				}

				delete ev.widget.queryResult[p.dataId].format;
				ev.widget.queryResult[p.dataId].text = newVal;
				if (ev.widget.queryResult.gauge) {
					delete ev.widget.queryResult.min.format;
					ev.widget.queryResult.min.text = newMin;
				
					delete ev.widget.queryResult.max.format;
					ev.widget.queryResult.max.text = newMax;
				}
			}
		});
	};

	//////////////////////////////////
	//	Initialization Functions	//
	//////////////////////////////////

	//	Function to determine if the menu items should be added
	function canEnableFormat(e,args,headerMenuCaption,menuType){

		//	Assume true by default
		var result = true;				
		try {
			//	Has the menu been added already?			
			$.each(args.settings.items, function(){
				if (this.caption === headerMenuCaption) {
					result = false;
				}
			});

			//	Only show this in the widget editor
			var widgetEditorOpen = (prism.$ngscope.appstate == "widget");
			if (!widgetEditorOpen) {
				result = false;
			}

			// find the widget
			var widget = e.currentScope.widget;
			if(widget == undefined){
				result = false;
			}

			// Widget must be an allowed chart
			if(!widgetIsSupported(widget.type)){
				result = false;
			}

			//	Check where the user clicked
			var target = args.ui.target;            
			if (menuType === 'measure') {
				//	Make sure the user clicked on a measures menu
				if (args.settings.name !== "widget-metadataitem") {
					result = false;
				}
				//	Make sure the settings menu is for a value
				if (args.settings.item.$$panel.name !== "value" &&
					args.settings.item.$$panel.name !== "secondary" 
				) {
					result = false;
				}
			} else if (menuType === 'settings') {
				//	Make sure the user clicked on a settings menu
				if (args.ui.css !== "w-menu-host") {
					result = false;
				}
			}

		} catch(e) {
			result = false;
		}

		//	Return result
		return result;
	}

	// Registering dashboard/ widget creation in order to perform drilling
	prism.on("dashboardloaded", function (e, args) {
		args.dashboard.on("widgetinitialized", onWidgetInitialized);
	});

	// register widget events upon initialization
	function onWidgetInitialized(dashboard, args) {
		//	Hooking to ready/destroyed events
		var shouldInit = widgetIsSupported(args.widget.type);
		//	Add hook for the quadrant analysis
		if (shouldInit) {
			//args.widget.on("ready", function(se, ev) {formatTime(se, ev)});

			args.widget.on("render", formatTime)
		}
	}

	prism.on("beforemenu", function (e, args) {

		//	Define the menu label
		var MENU_CAPTION = "Format Seconds";

		//	Can we show the options?
		var addMeasureMenuItem = canEnableFormat(e,args,MENU_CAPTION,'measure');

		if (addMeasureMenuItem) {
			//	Get the widget
			var widget = args.settings.item.$$panel.$$widget;	

			//	Look for a saved property
			var savedFormatType = args.settings.item[FORMAT_SPEC] ? args.settings.item[FORMAT_SPEC] : none;

			//	Function to create menus
			var formatTypeItems = function(panelItem, formatList) {
				//	Create an array to hold the list
				var items = [];

				//	Get the setting for this
				var selectedType = savedFormatType;
				
				//	Function the runs when an item is picked
				var itemPicked = function(){						
					//	Update the format setting
					this.panelItem[FORMAT_SPEC] = this.caption;
					//	Redraw the widget
					widget.redraw();						
				};

				//	Loop through each regression option and add sub-menu item
				$.each(formatList,function(){
					
					//	Get the option as a string
					var option = this.toString();

					//	Create the menu option
					var item = {
						caption: option,
						checked: (option === selectedType),
						size: 'xl',
						type: 'check',
						panelItem: panelItem,
						execute: itemPicked
					};

					//	Add to the list
					items.push(item);
				});

				//	Return the list of items				
				return items;
			};


			// Create the menu items
			for (var k in MENU_ITEMS) {
				var formatTypes = {
					caption: k,
					items: formatTypeItems(args.settings.item, MENU_ITEMS[k])
				};

				//	Add options to the menu
				args.settings.items.push(formatTypes);
			}
		}

	});
//}]);


